package com.example.springWithPostgresql.controller;

import com.example.springWithPostgresql.model.Role;
import com.example.springWithPostgresql.model.User;
import com.example.springWithPostgresql.model.UserRole;
import com.example.springWithPostgresql.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin("*")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User createUser(@RequestBody User user) throws Exception {
        Set<UserRole> userRoles = new HashSet<>();
        Role role = new Role();
        role.setRoleId(45L);
        role.setRoleName("NORMAL");
        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);

        userRoles.add(userRole);
        return  this.userService.createUser(user,userRoles);
    }

    @GetMapping("/{userName}")
    public User getUser(@PathVariable("userName") String userName){
        return  this.userService.getUser(userName);
    }

    @GetMapping("/showAll")
    public List<User> getAllUsers(){
        List<User> allUsers = this.userService.getAllUsers();
        return  allUsers;
    }


    @PutMapping("/update/{id}")
    public User updateUser(@PathVariable("id") Long id, @RequestBody(required = false) User user) throws Exception {
        User userbyId = this.userService.getById(id);
        Set<UserRole> userroles = userbyId.getUserroles();
        if (user != null) {
            user.setId(userbyId.getId());
            return  this.userService.updatingUser(user,userroles);
        }else {
            return  this.userService.updatingUser(userbyId,userroles);
        }

    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable("id")Long id){
        if(this.userService.getById(id) != null){
            this.userService.deleteById(id);
            System.out.println("seccessfully deleted ");
        }else {
            System.out.println("user not exist");
        }

    }
}
