package com.example.springWithPostgresql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "district")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class District {
    private Long districtId;
    private String districtName;

    @OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY , mappedBy = "district")
    private Set<User> userList = new HashSet<>();
}
