package com.example.springWithPostgresql.services;

import com.example.springWithPostgresql.model.User;
import com.example.springWithPostgresql.model.UserRole;

import java.util.List;
import java.util.Set;

public interface UserService {

    //creating user
    public User createUser(User user, Set<UserRole> userRoles) throws Exception;

    //Update user
    public User updatingUser(User user, Set<UserRole> userRoles) throws Exception;

    //get user by user name
    public User getUser(String username);

    //get user by user id
    public User getById(Long id);

    //Delete user by user id
    public void deleteById(Long id);

    public List<User> getAllUsers();
}
