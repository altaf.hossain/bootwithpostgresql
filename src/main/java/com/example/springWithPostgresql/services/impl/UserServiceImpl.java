package com.example.springWithPostgresql.services.impl;

import com.example.springWithPostgresql.model.User;
import com.example.springWithPostgresql.model.UserRole;
import com.example.springWithPostgresql.repo.RoleRepository;
import com.example.springWithPostgresql.repo.UserRepository;
import com.example.springWithPostgresql.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;



    @Override
    public User createUser(User user, Set<UserRole> userRoles) throws Exception {
        User local = this.userRepository.findByUserName(user.getUserName());
        if (local != null){
            System.out.println("User already exist");
            throw new Exception("User already exist");
        }else {
            for(UserRole ur : userRoles){
                roleRepository.save(ur.getRole());
            }
            user.getUserroles().addAll(userRoles);
            local = this.userRepository.save(user);
        }
        return local;
    }

    @Override
    public User updatingUser(User user, Set<UserRole> userRoles) throws Exception {
        User local = this.userRepository.findByUserName(user.getUserName());
        if (local != null){
            System.out.println("User Remain unchanged");
        }else {
            for(UserRole ur : userRoles){
                roleRepository.save(ur.getRole());
            }
            user.getUserroles().addAll(userRoles);
            local = this.userRepository.save(user);
        }
        return local;
    }

    @Override
    public User getUser(String username) {
        return this.userRepository.findByUserName(username);
    }

    @Override
    public User getById(Long id) {
        User user = this.userRepository.findById(id).get();
        return user;
    }

    @Override
    public void deleteById(Long id) {
        this.userRepository.deleteById(id);
    }


    @Override
    public List<User> getAllUsers() {
        List<User> all = this.userRepository.findAll();
        return all;
    }


}
